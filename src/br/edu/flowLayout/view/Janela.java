package br.edu.flowLayout.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Janela extends JFrame {

	private FlowLayout layout;
	private Container container;
	private JButton leftButton;
	private JButton centerButton;
	private JButton rightButton;
	
	public Janela() {
		super("Exemplo do FlowLayout");
		layout = new FlowLayout();
		container = getContentPane();
		setLayout(layout);
		
		leftButton=new JButton("Esquerda");
		leftButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				layout.setAlignment(FlowLayout.LEFT);
				layout.layoutContainer(container);
			}
			
		});
		
		centerButton=new JButton("Centro");
		centerButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				layout.setAlignment(FlowLayout.CENTER);
				layout.layoutContainer(container);
			}
						
		});
		
		rightButton = new JButton("Direita");
		rightButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				layout.setAlignment(FlowLayout.RIGHT);
				layout.layoutContainer(container);
				
			}
			
		});
		
		add(leftButton);
		add(centerButton);
		add(rightButton);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400,200);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		Janela jl=new Janela();
	}

}
